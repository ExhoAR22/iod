#ifndef __FUNCS_H
#define __FUNCS_H
#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/extensions/XTest.h>
#include <unistd.h>

void set_cursor(int x, int y);
void send_keysym(KeySym k);
void click(int button);
#endif