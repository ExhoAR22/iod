#include "funcs.h"

void set_cursor(int x, int y) {
    Display* display;
    Window root_window;
    display = XOpenDisplay(0);
    root_window = XRootWindow(display, 0);
    XSelectInput(display, root_window, KeyReleaseMask);
    XWarpPointer(display, 0, root_window, 0, 0, 0, 0, x, y);
    XFlush(display);
    XCloseDisplay(display);
}

void send_keysym(KeySym k) {
    Display *dis;
    dis = XOpenDisplay(NULL);
    KeyCode modcode = XKeysymToKeycode(dis, k);
    XTestFakeKeyEvent(dis, modcode, False, 0);
    XFlush(dis);
    XTestFakeKeyEvent(dis, modcode, True, 0);
    XFlush(dis);
    XTestFakeKeyEvent(dis, modcode, False, 0);
    XFlush(dis);
    XCloseDisplay(dis);
}

void click(int button) {
    Display* display;
    XEvent event;
    display = XOpenDisplay(NULL);
    event.xbutton.button = button;
    event.xbutton.same_screen = True;
    event.xbutton.subwindow = DefaultRootWindow(display);
    while (event.xbutton.subwindow) {
        event.xbutton.window = event.xbutton.subwindow;
        XQueryPointer(display, event.xbutton.window, &event.xbutton.root, &event.xbutton.subwindow,
		    &event.xbutton.x_root, &event.xbutton.y_root, &event.xbutton.x, &event.xbutton.y,
		    &event.xbutton.state);
    }
    event.type = ButtonPress;
    if (XSendEvent(display, PointerWindow, True, ButtonPressMask, &event)) {
        XFlush(display);
        usleep(1);
        event.type = ButtonRelease;
        if (XSendEvent(display, PointerWindow, True, ButtonReleaseMask, &event)) {
            XFlush(display);
        }
    }
    XCloseDisplay(display);
}