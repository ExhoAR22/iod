#include "funcs.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <signal.h>
#define BUFFSIZE 1024
#define CMD_MOVE 0
#define CMD_KEY 1
#define CMD_CLICK 2
#define NI_MSG(x) printf("iod: %s is not implemented yet.\n", #x)

int serverfd = -1, clientfd = -1;

void exitproc() {
    puts("iod: Exiting...");
    if (clientfd > -1) {
        close(clientfd);
    }
    if (serverfd > -1) {
        close(serverfd);
    }
}

void onint(int sig) {
    exit(0);
}

void handle(uint8_t* cmd, int length) {
    if (!length) {
        return;
    }
    if (*cmd == CMD_MOVE) {
        set_cursor(*((uint16_t*)(cmd + 1)), ((uint16_t*)(cmd + 1))[1]);
    }
    else if (*cmd == CMD_KEY) {
        send_keysym(*((KeySym*)(cmd + 1)));
    }
    else if (*cmd == CMD_CLICK) {
        click(*((int*)(cmd + 1)));
    }
}

int main(int argc, char* argv[], char* envp[]) {
    uint8_t buff[BUFFSIZE] = { 0 };
    if (argc <= 1) {
        printf("Useage: %s <port>\n", argv[0]);
        return 0;
    }
    atexit(exitproc);
    signal(SIGINT, onint);
    struct sockaddr_in serv_addr;
    if ((serverfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        puts("iod: Unable to create socket.");
        return errno;
    }
    bzero((char*)&serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(atoi(argv[1]));
    if (bind(serverfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        close(serverfd);
        puts("iod: Unable to bind.");
        return errno;
    }
    while (1) {
        if (listen(serverfd, 1)) {
            puts("iod: Unable to listen.");
            return errno;
        }
        if ((clientfd = accept(serverfd, NULL, NULL)) < 0) {
            puts("iod: Unable to accept.");
            return errno;
        }
        handle(buff, read(clientfd, buff, BUFFSIZE));
        close(clientfd);
        clientfd = -1;
    }
}