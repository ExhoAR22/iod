.PHONY: all clean test

SOURCES := $(wildcard src/*.c)
OBJECTS := $(patsubst src/%.c, bin/obj/%.o, $(SOURCES))

all: bin/iod
clean:
	rm -r bin

bin/obj/%.o: src/%.c
	test -d `dirname $@` || mkdir -p `dirname $@`
	gcc -Iinc -c $< -o $@

bin/iod: $(OBJECTS)
	test -d `dirname $@` || mkdir -p `dirname $@`
	gcc -o $@ $^ -lX11 -lXtst